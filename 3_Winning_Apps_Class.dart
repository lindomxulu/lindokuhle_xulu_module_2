void main() {
  var AppoftheYear2012 = new AppoftheYear();
  AppoftheYear2012.AppName = "FNB Banking App";
  AppoftheYear2012.AppCategory_Sector =
      "Best IOS App (Consumer), Best Android App (Consumer)";
  AppoftheYear2012.AppDeveloper = "Frederick Kobo";
  AppoftheYear2012.AppYearWon = 2012;

  var AppoftheYear2013 = new AppoftheYear();
  AppoftheYear2013.AppName = "SnapScan";
  AppoftheYear2013.AppCategory_Sector = "Best HTML 5 App";
  AppoftheYear2013.AppDeveloper = "Kobus Ehlers";
  AppoftheYear2013.AppYearWon = 2013;

  var AppoftheYear2014 = new AppoftheYear();
  AppoftheYear2014.AppName = "LIVE Inspect";
  AppoftheYear2014.AppCategory_Sector = "Best Android App (Enterprise)";
  AppoftheYear2014.AppDeveloper = "Lightstone";
  AppoftheYear2014.AppYearWon = 2014;

  var AppoftheYear2015 = new AppoftheYear();
  AppoftheYear2015.AppName = "WumDrop";
  AppoftheYear2015.AppCategory_Sector = "Best Enterprise Solution";
  AppoftheYear2015.AppDeveloper = "Roy Borole, Simon Hartley";
  AppoftheYear2015.AppYearWon = 2015;

  var AppoftheYear2016 = new AppoftheYear();
  AppoftheYear2016.AppName = " Domestly";
  AppoftheYear2016.AppCategory_Sector = "Overall Winner";
  AppoftheYear2016.AppDeveloper = "Berno Potgieter, Thatoyaona Marumo";
  AppoftheYear2016.AppYearWon = 2016;

  var AppoftheYear2017 = new AppoftheYear();
  AppoftheYear2017.AppName = "Shyft for Standard Bank";
  AppoftheYear2017.AppCategory_Sector = "Best Financial Solution";
  AppoftheYear2017.AppDeveloper = "Standard Bank";
  AppoftheYear2017.AppYearWon = 2017;

  var AppoftheYear2018 = new AppoftheYear();
  AppoftheYear2018.AppName = "Khula";
  AppoftheYear2018.AppCategory_Sector = "Best Agricultural Solution";
  AppoftheYear2018.AppDeveloper = "Matthew Piper";
  AppoftheYear2018.AppYearWon = 2018;

  var AppoftheYear2019 = new AppoftheYear();
  AppoftheYear2019.AppName = "Naked Insurance";
  AppoftheYear2019.AppCategory_Sector = "Best Financial Solution";
  AppoftheYear2019.AppDeveloper = "Alex Thomson";
  AppoftheYear2019.AppYearWon = 2019;

  var AppoftheYear2020 = new AppoftheYear();
  AppoftheYear2020.AppName = "EasyEquities";
  AppoftheYear2020.AppCategory_Sector = "Best Consumer Solution";
  AppoftheYear2020.AppDeveloper = "John Doe";
  AppoftheYear2020.AppYearWon = 2020;

  var AppoftheYear2021 = new AppoftheYear();
  AppoftheYear2021.AppName = "Ambani Africa";
  AppoftheYear2021.AppCategory_Sector =
      "Best Gaming Solution, Best Educational Solution, Best South African App";
  AppoftheYear2021.AppDeveloper = "Mukundi Lambani";
  AppoftheYear2021.AppYearWon = 2021;

  AppoftheYear2012.printDeveloperAllCaps();
  AppoftheYear2013.printDeveloperAllCaps();
  AppoftheYear2014.printDeveloperAllCaps();
  AppoftheYear2015.printDeveloperAllCaps();
  AppoftheYear2016.printDeveloperAllCaps();
  AppoftheYear2017.printDeveloperAllCaps();
  AppoftheYear2018.printDeveloperAllCaps();
  AppoftheYear2019.printDeveloperAllCaps();
  AppoftheYear2020.printDeveloperAllCaps();
  AppoftheYear2021.printDeveloperAllCaps();
}

class AppoftheYear {
  String? AppName;
  String? AppCategory_Sector;
  String? AppDeveloper;
  int? AppYearWon;

  void printDeveloperInformation() {
    print("App name is $AppName");
    print("The app category or sector is $AppCategory_Sector");
    print("The app developer is $AppDeveloper ");
    print("The app won MTN Business App of the Year Awards in $AppYearWon");
  }

  void printDeveloperAllCaps() {
    print(AppName?.toUpperCase());
    print("The app category or sector is $AppCategory_Sector");
    print("The app developer is $AppDeveloper ");
    print("The app won MTN Business App of the Year Awards in $AppYearWon");
  }
}
