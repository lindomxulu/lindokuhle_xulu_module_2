void main() {
  var AppoftheYear2012 = [
    "HealthID",
    "FNB Banking App",
    "TransUnion Dealer's Guide",
    "RapidTargets",
    "Matchy",
    "phraZapp",
    "Plascon Inspire Me"
  ];

  var AppoftheYear2013 = [
    "SnapScan",
    "DStv",
    ".comm Telco Data Visualizer",
    "PriceCheck Mobile",
    "Nedbank App Suite",
    "MarkitShare",
    "Kids Aid",
    "Gautrain Buddy",
    "bookly"
  ];

  var AppoftheYear2014 = [
    "SuperSport",
    "SyncMobile",
    "My Belongings",
    "LIVE Inspect",
    "Vigo",
    "Zapper",
    "Rea Vaya",
    "Wildlife Tracker"
  ];

  var AppoftheYear2015 = [
    "WumDrop",
    "Vula Mobile",
    "CPUT Mobile",
    "EskomSePush",
    "DStv Now",
    "M4JAM"
  ];

  var AppoftheYear2016 = [
    "Domestly",
    "KaChing",
    "Friendly Math Monsters for Kindergarten",
    "iKhokha",
    "HearZA",
    "Tuta-me"
  ];

  var AppoftheYear2017 = [
    "Shyft for Standard Bank",
    "InterGreatMe",
    "WatIf Health Portal",
    "Pick 'n Pay's Super Animals 2",
    "The TreeApp South Africa",
    "Awethu Project",
    "Hey Jude",
    "OrderIN",
    "The ORU Social and TouchSA",
    "Zulzi",
    "EcoSlips",
    "TransUnion 1Check"
  ];

  var AppoftheYear2018 = [
    "Khula",
    "SI Realities",
    "dbTrack",
    "The African Cyber Gaming League (ACGL)",
    "Xander English 1-20",
    "Ctrl",
    "Stokfella",
    "Pineapple",
    "Difela Hymns",
    "ASI Snakes",
    "Bestee",
    "Cowa Bunga"
  ];

  var AppoftheYear2019 = [
    "SI Realities",
    "Vula Mobile",
    "Lost Defence",
    "Hydra",
    "Matric Live",
    "Naked Insurance",
    "Franc",
    "Over",
    "My Pregnancy",
    "Journal",
    "Bottles",
    "LocTransie",
    "Digger",
    "Mo Wash"
  ];

  var AppoftheYear2020 = [
    "EasyEquities",
    "Checkers Sixty60",
    "BirdPro",
    "Lexie Hearing",
    "League of Legends",
    "GreenFingers Mobile",
    "Xitsonga Dictionary",
    "StokFella",
    "Bottles",
    "Examsta",
    "Technishen",
    "Guardian Health",
    "My Pregnancy Journey",
    "Matric Live"
  ];

  var AppoftheYear2021 = [
    "iiDENTIFii",
    "Hellopay SoftPOS",
    "Guardian Health",
    "Platform",
    "Ambani Africa",
    "Murimi",
    "Shyft",
    "Sisa",
    "UniWise",
    "Kazi",
    "Takealot",
    "Rekindle Learning",
    "Roadsave",
    "Afrihost"
  ];

  print("2012 Winners: $AppoftheYear2012");
  print("2013 Winners: $AppoftheYear2013");
  print("2014 Winners: $AppoftheYear2014");
  print("2015 Winners: $AppoftheYear2015");
  print("2016 Winners: $AppoftheYear2016");
  print("2017 Winners: $AppoftheYear2017");
  print("2018 Winners: $AppoftheYear2018");
  print("2019 Winners: $AppoftheYear2019");
  print("2020 Winners: $AppoftheYear2020");
  print("2021 Winners: $AppoftheYear2021");

  AppoftheYear2012.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2012);

  AppoftheYear2013.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2013);

  AppoftheYear2014.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2014);

  AppoftheYear2015.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2015);

  AppoftheYear2016.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2016);

  AppoftheYear2017.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2017);

  AppoftheYear2018.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2018);

  AppoftheYear2019.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2019);

  AppoftheYear2020.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2020);

  AppoftheYear2021.sort((a, b) {
    return a.compareTo(b);
  });
  print(AppoftheYear2021);

  print("Winning App for 2017 : Shyft for Standard Bank");
  print("Winning App for 2018 : Khula");

  print(AppoftheYear2012.length);
  print(AppoftheYear2013.length);
  print(AppoftheYear2014.length);
  print(AppoftheYear2015.length);
  print(AppoftheYear2016.length);
  print(AppoftheYear2017.length);
  print(AppoftheYear2018.length);
  print(AppoftheYear2019.length);
  print(AppoftheYear2020.length);
  print(AppoftheYear2021.length);

  var total_apps = AppoftheYear2012.length +
      AppoftheYear2013.length +
      AppoftheYear2014.length +
      AppoftheYear2015.length +
      AppoftheYear2016.length +
      AppoftheYear2017.length +
      AppoftheYear2018.length +
      AppoftheYear2019.length +
      AppoftheYear2020.length +
      AppoftheYear2021.length;

  print(total_apps);
}
